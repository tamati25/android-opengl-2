package com.tama.labo.opengl2.opengl;
import javax.microedition.khronos.opengles.GL10;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.*;

public class MyRenderer implements GLSurfaceView.Renderer
{
	private float[] mProjMatrix = new float[16];

    public MyRenderer()
	{
	}

	public void onSurfaceCreated(GL10 p1, EGLConfig p2)
	{
		p1.glClearColor(0.4f, 0.2f, 0.2f, 1.0f);
	}

	public void onDrawFrame(GL10 p1)
	{
		p1.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}
  
    public void onSurfaceChanged(GL10 p1, int width, int height)
    {
	    float ratio = width/height;
	    GLES20.glViewport(0, 0, height, width); //landscape mode
		Matrix.orthoM(mProjMatrix, 0, 0, width, 0, height, -1, 1);
		
    }
}
