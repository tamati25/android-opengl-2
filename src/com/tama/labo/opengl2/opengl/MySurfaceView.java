package com.tama.labo.opengl2.opengl;

import android.opengl.GLSurfaceView;
import android.content.Context;
import android.util.AttributeSet;

public class MySurfaceView extends GLSurfaceView
{
  private MyRenderer mRenderer;

  public MySurfaceView(Context context, AttributeSet attrSet)
  {
	  super(context, attrSet);
	  mRenderer = new MyRenderer();
	  setRenderer(mRenderer);
  }
}
